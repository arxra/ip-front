FROM node:13.8

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install 
COPY . .
RUN npm run build


# Now deploy
FROM nginx:mainline-alpine

WORKDIR /usr/share/nginx/html
COPY --from=0 /app/public /usr/share/nginx/html

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
